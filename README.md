# iNKT barcoding study

Authors:

- Malte Petersen
- Norimasa Iwanami
- Thomas Boehm

This repository contains the analysis and results for the iNKT barcoding study. All analysis is done in the RMarkdown file `report.Rmd`, which will knit smoothly to a HTML document that includes all figures. It will also save the figures and tables in the respective directories.

Required input:

- The YAML file `config/config.yaml` contains configuration: all file paths and analysis parameters.
- The XLSX file `doc/Bulk_BC_data_Nori_only_MP.xlsx` lists all sample information.
- The TSV (tab-separated) file `config/units.tsv` lists the input FASTQ file paths for each sample.

(I know, I know, the sample sheet could also be a TSV/CSV file, but it was simpler to use XLSX with my collaborators)
 
 Most of the analysis should run without changing anything, but there are a few places where I had to hardcode some sample properties. If you are reproducing this analysis with your own data, please have a look at those sections and amend or take them out.
